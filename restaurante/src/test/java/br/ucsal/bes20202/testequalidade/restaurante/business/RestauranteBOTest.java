package br.ucsal.bes20202.testequalidade.restaurante.business;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import br.ucsal.bes20202.testequalidade.builder.MesaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 * 3. NÃO é necesário fazer o mock para o construtor de Comanda;
	 * 
	 * 4. Como o método abrirComanda é void, você deverá usar o verify para se
	 * certificar do sucesso o não da execução do teste. Não é necessário garantir
	 * que a comanda incluída é para a mesa informada, basta verificar se uma
	 * comanda foi incluída.
	 * 
	 */
	
	public static MesaBuilder builderMesa;
	public static Mesa mesa1;
	public static MesaDao mesaDaoMock;
	public static ComandaDao comandaDaoMock;
	public static ItemDao itemDaoMock;
	public static RestauranteBO restauranteBO;
	
	@BeforeAll
	public static void setup() throws RegistroNaoEncontrado, MesaOcupadaException {
		builderMesa = MesaBuilder.mesaDefault();
		mesa1 = builderMesa.but().build();
		mesaDaoMock = Mockito.mock(MesaDao.class);
		comandaDaoMock = Mockito.mock(ComandaDao.class);
		itemDaoMock = Mockito.mock(ItemDao.class);
		
		Mockito.when(mesaDaoMock.obterPorNumero(Mockito.anyInt())).thenReturn(mesa1);
		
		restauranteBO = new RestauranteBO(mesaDaoMock, comandaDaoMock, itemDaoMock);
		restauranteBO.abrirComanda(mesa1.getNumero());
		
	}
	
	@Test
	public void abrirComandaMesaLivre(){
		Mockito.verify(comandaDaoMock).incluir(Mockito.any());
	}
}
