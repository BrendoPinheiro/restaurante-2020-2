package br.ucsal.bes20202.testequalidade.restaurante.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20202.testequalidade.builder.MesaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOIntegradoTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. Como o método abrirComanda é void, você deverá utilizar o ComandaDAO para
	 * verificar se o método teve sucesso. É necessário verificar se a comanda foi
	 * incluída para a mesa informada.
	 * 
	 */
	
	public static MesaBuilder builderMesa;
	private static Mesa mesa1;
	
	private static MesaDao mesaDAO = new MesaDao();
	private static ItemDao itemDAO = new ItemDao();
	private static ComandaDao comandaDAO = new ComandaDao();
	
	
	@BeforeAll
	public static void setup() throws RegistroNaoEncontrado, MesaOcupadaException {
		builderMesa = MesaBuilder.mesaDefault();
		mesa1 = builderMesa.but().build();
		mesaDAO.incluir(mesa1);
		
		RestauranteBO restauranteBO = new RestauranteBO(mesaDAO, comandaDAO, itemDAO);
		
		restauranteBO.abrirComanda(mesa1.getNumero());
		restauranteBO.abrirComanda(mesa1.getNumero());
	}
	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado {
		int resultadoAtual = comandaDAO.obterComandasPorNumeroMesa(0).size();
		int resultadoEsperado = 2;
		assertEquals(resultadoEsperado, resultadoAtual);
	}
}
