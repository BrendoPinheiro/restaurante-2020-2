package br.ucsal.bes20202.testequalidade.builder;

import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaBuilder {
	public static final Integer DEFAULT_NUMERO = 0;
	public static final Integer DEFAULT_CAPACIDADE = 4;
	public static final SituacaoMesaEnum DEFAULT_SITUACAO = SituacaoMesaEnum.LIVRE;
	
	private Integer numero = DEFAULT_NUMERO;
	private Integer capacidade = DEFAULT_CAPACIDADE;
	private SituacaoMesaEnum situacao = DEFAULT_SITUACAO;
	
	private MesaBuilder() {};
	
	public static MesaBuilder mesa() {
		return new MesaBuilder();
	}
	
	public static MesaBuilder mesaDefault() {
		return MesaBuilder.mesa()
				.withNumero(DEFAULT_NUMERO)
				.withCapacidade(DEFAULT_CAPACIDADE)
				.withSituacao(DEFAULT_SITUACAO);
	}
	
	public MesaBuilder withNumero(Integer numero) {
		this.numero = numero;
		return this;
	}
	
	public MesaBuilder withCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
		return this;
	}
	
	public MesaBuilder withSituacao (SituacaoMesaEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public MesaBuilder but() {
		return MesaBuilder
				.mesa()
				.withNumero(this.numero)
				.withCapacidade(this.capacidade)
				.withSituacao(this.situacao);
	}
	
	public Mesa build() {
		Mesa instance = new Mesa();
		instance.setNumero(this.numero);
		instance.setCapacidade(this.capacidade);
		instance.setSituacao(this.situacao);
		return instance;
	}
	
	
}
